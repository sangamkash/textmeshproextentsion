﻿using System.Collections;
using System.Collections.Generic;
using MagnasStudio.Utility.TMPro;
using TMPro;
using UnityEngine;

public class TMProExtensionExample : MonoBehaviour
{
   [SerializeField] private TextMeshProUGUI t1, t2, t3, t4;
   [SerializeField] private TextMeshProUGUI t5;

   public void Start()
   {
      t1.text = $" {"red".TMProColorTag("FF0000")} {"bold".TMProBoldTag()} {"UnderLine".TMProUnderLineTag()} {"Strike".TMProStrikeTag()} {"Italic".TMProItalicTag()}";
      t2.text = $"{"LowerCase".TMProLowerCaseTag()} {"UpperCase".TMProUpperCaseTag()} {"smallCap".TMProSmallCapsTag()}";
      t3.text = $"{"Alpha".TMProAlphaTag("AA")} {"HighLight".TMProHigLightTag("00AAAAAA")} {"www.google.com".TMProHyperLinkTag("www.google.com")}";
      t4.text = $"{"blackOutLine".TMProMatTag(MaterialType.LP_OutLine_Black)} {"YellowOutLine".TMProMatTag(MaterialType.LP_OutLine_Yellow)} {"Different font with material".TMProFontAndMatTag(FontType.AO, MaterialType.AO_OutLine_Black)}";
      //Incorrect Way
      t5.text =$"{"Wrong".TMProMatTag(MaterialType.AO_OutLine_Black)} {"Again IncorrectWay".TMProFontAndMatTag(FontType.LP, MaterialType.AO_OutLine_Black)}";
   }
}
