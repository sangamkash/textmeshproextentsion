﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MagnasStudio.Utility.TMPro.Core.Editor
{
    [System.Serializable]
    public class MatData
    {
        public FontType fontType;
        public string matPath;
    }
    [CreateAssetMenu(fileName = "TMProAssetsData", menuName = "MagnasStudio/TMProAssetsData", order = 1)]
    public class TMProAssetsData : ScriptableObject
    {
        [SerializeField] private TMProFontData[] fontDatas;
        private const string path = "TMProAssetsData";
        private Dictionary<FontType, string> fontDic;
        private Dictionary<MaterialType, MatData> materialDic;
        private static TMProAssetsData _Instance;
        public static TMProAssetsData Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = Resources.Load<TMProAssetsData>(path);
                    _Instance.Init();
                }
                return _Instance;
            }
        }

        private void Init()
        {
            fontDic = new Dictionary<FontType, string>();
            materialDic= new Dictionary<MaterialType, MatData>();
            for (var i = 0; i < fontDatas.Length; i++)
            {
                var type = (FontType) Enum.Parse(typeof(FontType), fontDatas[i].shortName);
                if (fontDic.ContainsKey(type))
                {
                    Debug.LogError($"duplicate fontType found {type.ToString()}");
                }
                else
                {
                    fontDic.Add(type,fontDatas[i].fontAssetPath);
                    var count = fontDatas[i].materialPaths.Count;
                    for (var j = 0; j < count; j++)
                    {
                        var str = fontDatas[i].shortName + "_" + fontDatas[i].materialPaths[j].name;
                        var matType = (MaterialType) Enum.Parse(typeof(MaterialType), str);
                        if (materialDic.ContainsKey(matType))
                        {
                            Debug.LogError($"duplicate MaterialType found {matType.ToString()}");
                        }
                        else
                        {
                            var matData = new MatData()
                            {
                                fontType = type,
                                matPath = fontDatas[i].materialPaths[j].path
                            };
                            materialDic.Add(matType,matData);
                        }
                    }
                }
            }
        }

        public string GetPathOfFontType(FontType fontType)
        {
            return fontDic[fontType];
        }
        
        public string GetPathOfMaterialType(MaterialType materialType)
        {
            return materialDic[materialType].matPath;
        }

        public bool IsValidateMaterial(FontType fontType,MaterialType materialType)
        {
            return materialDic[materialType].fontType == fontType;
        }

#if UNITY_EDITOR
        public void UpdateFontData(TMProFontData[] fontDatas)
        {
            this.fontDatas = fontDatas;
        }
#endif
    }
}