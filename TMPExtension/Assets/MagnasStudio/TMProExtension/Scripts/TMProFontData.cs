﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using TMPro;
using Object = UnityEngine.Object;

namespace MagnasStudio.Utility.TMPro.Core
{
    [System.Serializable]
    public class MaterialPath
    {
#if UNITY_EDITOR
        public Material material;
#endif
        public string name;
        public string path;
    }
    
    [CreateAssetMenu(fileName = "TMProFontData", menuName = "MagnasStudio/TMProFontData", order = 1)]
    public class TMProFontData : ScriptableObject 
    {
        [Tooltip("recommended length is 6 or less")]
        public string shortName;
#if UNITY_EDITOR
        public TMP_FontAsset fontAssets;
#endif
        public string fontAssetPath;
        public List<MaterialPath> materialPaths;

#if UNITY_EDITOR
        [ContextMenu("AssignPath")]
        private void AssignPath()
        {
            shortName = shortName.Trim();
            var count = materialPaths.Count;
            for (int i = 0; i < count; i++)
            {
                if (!GetResourcePath(materialPaths[i].material, out materialPaths[i].path))
                {
                    Debug.LogError($"path not found");
                }
            }
            if (!GetResourcePath(fontAssets, out fontAssetPath))
            {
                Debug.LogError($"path not found");
            }
        }
        private bool GetResourcePath(Object obj, out string resourcePath)
        {
            resourcePath = "";
            if (obj == null)
            {
                Debug.LogError("Object is null");
                return false;
            }

            var path = UnityEditor.AssetDatabase.GetAssetPath(obj);
            if (path.Contains("Resources"))
            {
                var newpath = "";
                string[] splits = path.Split('/');
                int status = 0;
                for (int j = 0; j < splits.Length; j++)
                {
                    if (splits[j] == "Resources")
                    {
                        status = 1;
                    }
                    else if (status == 1)
                    {
                        newpath += splits[j] + "/";
                    }

                }

                splits = newpath.Split('.');
                resourcePath = splits[0];
                return true;
            }
            else
            {
                Debug.LogError(
                    $"GameObject {obj.name} must need to be added in <color=yellow>Resources</color> folder");
            }

            return false;
        }

        [ContextMenu("Validate")]
        private void ValidateData()
        {
            if (materialPaths == null)
                return;
            var count = materialPaths.Count;
            shortName = shortName.Trim();
            if (string.IsNullOrEmpty(shortName))
            {
                Debug.LogError($"shortName cannot be null for fortData {this.name}");
            }
            else if(shortName.Length>6)
            {
                Debug.LogWarning($"shortName is recommended with in under 6 character {this.name}");
            }
            // var remove=new List<SceneAsset>();
            for (var i = 0; i < count; i++)
            {
                if (materialPaths[i].material == null)
                {
                    materialPaths.Remove(materialPaths[i]);
                    count--;
                    i--;
                    continue;
                }

                for (int j = i + 1; j < count; j++)
                {
                    if (materialPaths[i].material == materialPaths[j].material)
                    {
                        materialPaths.Remove(materialPaths[j]);
                        count--;
                        i--;
                        break;
                    }
                }
            }
        }

        public void Refresh()
        {
            ValidateData();
            AssignPath();
        }
        
#endif
    }
}
