﻿using System.Collections;
using System.Collections.Generic;
using MagnasStudio.Utility.TMPro.Core.Editor;
using UnityEngine;

namespace MagnasStudio.Utility.TMPro
{

    public static class TMProTagExtension
    {
        /// <summary>
        /// returns the resource path of materialType
        /// </summary>
        /// <param name="materialType"></param>
        /// <returns></returns>
        public static string GetMatAddress(MaterialType materialType)
        {
            return TMProAssetsData.Instance.GetPathOfMaterialType(materialType);
        }

        /// <summary>
        /// returns the resource path of fontType
        /// </summary>
        /// <param name="fontType"></param>
        /// <returns></returns>
        public static string GetFontAddress(FontType fontType)
        {
            return TMProAssetsData.Instance.GetPathOfFontType(fontType);
        }

        public static bool IsValidateMaterial(FontType fontType, MaterialType materialType)
        {
            return TMProAssetsData.Instance.IsValidateMaterial(fontType, materialType);
        }
        
        /// <summary>
        /// Changes text to italic style
        /// </summary>
        /// <returns></returns>
        public static string TMProItalicTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<i>{str}</i>";
        }
        
        /// <summary>
        /// Changes text to bold   
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TMProBoldTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<b>{str}</b>";
        }
        
        /// <summary>
        /// Changes text to underLine text   
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TMProUnderLineTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<u>{str}</u>";
        }
        
        /// <summary>
        /// strikeThrough text to strike text 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TMProStrikeTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<s>{str}</s>";
        }
        
        /// <summary>
        /// Changes text to lowerCase   
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TMProLowerCaseTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<lowercase>{str}</lowercase>";
        }
        
        /// <summary>
        /// Changes text to upperCase   
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TMProUpperCaseTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<uppercase>{str}</uppercase>";
        }
        
        /// <summary>
        /// Changes text to SmallCaps   
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TMProSmallCapsTag(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<smallcaps>{str}</smallcaps>";
        }

        /// <summary>
        /// Changes text color ::
        /// Example : TMProColor("FFFFFF")
        /// </summary>
        /// <param name="hexCode">000000-FFFFFF</param>
        /// <returns></returns>
        public static string TMProColorTag(this string str, string hexCode)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<color=#{hexCode}>{str}</color>";
        }

        /// <summary>
        /// Changes text alpha ::
        /// Example : TMProAlphaTag("FF")
        /// </summary>
        /// <param name="hexCode">00 to FF</param>
        /// <returns></returns>
        public static string TMProAlphaTag(this string str, string hexCode)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<alpha=#{hexCode}>{str}<alpha=#FF>";
        }
        
        /// <summary>
        /// HighLight the text ::
        /// Example : TMProHigLightTag("FFFFFFFF"):::
        /// Input =>FF,FF,FF,FF
        /// </summary>
        /// <param name="str"></param>
        /// <param name="hexCode"> </param>
        /// <returns></returns>
        public static string TMProHigLightTag(this string str,string hexCode)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<mark=#{hexCode}>{str}</mark>";
        }
        
        /// <summary>
        /// Hyperlink the text 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        public static string TMProHyperLinkTag(this string str,string link)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<link=\"{link}\">{str}</link>";
        }
        
        /// <summary>
        /// Note : Make sure the material Should be supported tp font
        /// Changes text material   
        /// </summary>
        /// <param name="str"></param>
        /// <param name="materialType"></param>
        /// <returns></returns>
        public static string TMProMatTag(this string str, MaterialType materialType)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return $"<material=\"{GetMatAddress(materialType)}\">{str}</material>";
        }

        /// <summary>
        /// Note : Make sure the font and Material are of same fontAssets
        /// Changes text font and material 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="fontType"></param>
        /// <param name="materialType"></param>
        /// <returns></returns>
        public static string TMProFontAndMatTag(this string str, FontType fontType, MaterialType materialType)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            if (IsValidateMaterial(fontType, materialType))
            {
                return $"<font=\"{GetFontAddress(fontType)}\" material=\"{GetMatAddress(materialType)}\">{str}</font>";
            }
            else
            {
                Debug.LogError(
                    $"TAG invalid materialType : {materialType.ToString()} for font : {fontType.ToString()} ");
                return str;
            }
        }
    }
}
