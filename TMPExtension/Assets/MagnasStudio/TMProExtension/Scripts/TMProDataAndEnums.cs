namespace MagnasStudio.Utility.TMPro
{
 [System.Serializable]
 public enum FontType
 {
  LP,
  AO,
  OJ
 }

 [System.Serializable]
 public enum MaterialType
 {
  LP_OutLine_Black,
  LP_OutLine_Blue,
  LP_OutLine_Yellow,
  LP_Simple,
  AO_OutLine_Black,
  OJ_OutLine_Black
 }
}