﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using UnityEditor;
using UnityEngine;

namespace MagnasStudio.Utility.TMPro.Core.Editor
{
    [System.Serializable]
    public class TMProEditorData
    {
        private const string JsonFilePath = "MagnasStudio/Data/EditorData/TMproExtension";
        private const string FileName = "TMproExtension.json";
        public List<string> fontDataPaths;
        [NonSerialized]
        public List<TMProFontData> fontDatas;

        private static TMProEditorData _Instance;

        public static TMProEditorData Instance
        {
            get
            {
                if (_Instance == null)
                {
                    var obj = DeSerializedObject();
                    if (obj != null)
                        Debug.Log("<color=red>======DeSerializedObject fail DeleteJson file=====</color>");
                    _Instance = obj != null ? obj : new TMProEditorData();
                }

                return _Instance;
            }
        }
        private TMProEditorData()
        {
            fontDataPaths = new List<string>();
            fontDatas = new List<TMProFontData>();
        }

        public void Save()
        {
            var path = Application.dataPath + "/" + JsonFilePath;
#if UNITY_EDITOR_WIN
            var directoryPath = path.Replace("/", "\\");
#else
            var directoryPath = path;
#endif
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(directoryPath);
            }
            fontDataPaths = UpdateFontDataPaths();
            var filepath = path + "/" + FileName;
            var file = File.Open(filepath, FileMode.OpenOrCreate);
            var bytes = Encoding.UTF8.GetBytes(JsonUtility.ToJson(this,true));
            file.Write(bytes, 0, bytes.Length);
            file.Close();
        }
        private List<string> UpdateFontDataPaths()
        {
            List<string> paths=new List<string>();
            if (fontDatas == null)
                return paths;
            var count=fontDatas.Count;
            for (int i = 0; i < count; i++)
            {
                var path=AssetDatabase.GetAssetPath(fontDatas[i]);
                paths.Add(path);
            }
            return paths;
        }
        public static TMProEditorData DeSerializedObject()
        {
            var directory = Application.dataPath + "/" + JsonFilePath;
            if (!Directory.Exists(directory))
            {
                Debug.LogWarning("Directory don't exits {directory}");
                return null;
            }

            var filepath =directory + "/" + FileName;
            if (!File.Exists(filepath))
            {
                Debug.LogWarning($"File don't exit path {filepath}");
                return null;
            }

            var text = File.ReadAllText(filepath);

            TMProEditorData data;
            try
            {
                data = JsonUtility.FromJson<TMProEditorData>(text);
                data.fontDatas = new List<TMProFontData>();
                var count = data.fontDataPaths.Count;
                for (var i = 0; i < count; i++)
                {
                    var temp = AssetDatabase.LoadAssetAtPath<TMProFontData>(data.fontDataPaths[i]);
                    if (temp != null)
                        data.fontDatas.Add(temp);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("Exception +++++++++++++++++++++++++++++++++++++++++++++++++++++++" + ex.StackTrace);
                return null;
            }
            return data;
        }
    }
}

