﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
#if UNITY_EDITOR
using System.IO;
#endif
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace MagnasStudio.Utility.TMPro.Core.Editor
{
    public class TMProExtensionWindow : EditorWindow
    {
        private static TMProExtensionWindow Instance;

        private static List<TMProFontData> fontDatas
        {
            get
            {
                return TMProEditorData.Instance.fontDatas;
            }
            set
            {
                TMProEditorData.Instance.fontDatas = value;
            }
        }
        private const string path = "/MagnasStudio/TMProExtension/Scripts";
        private const string assetPath = "/MagnasStudio/TMProExtension/Resources";
        private Vector2 scrollPos;
        
        private void Save() => TMProEditorData.Instance.Save();
        
        [MenuItem("Window/MagnasStudio/TMProExtension")]
        public static void ShowWindow()
        {
            Instance = (TMProExtensionWindow) EditorWindow.GetWindow(typeof(TMProExtensionWindow), true,
                "TMProExtension");
            Instance.ShowPopup();
        }

        private void OnGUI()
        {
            var w = position.width;
            var h = position.height;
            var cellHeight = 30;
            var buttonHeight = 30;
            GUILayout.Label("AssigneFontData", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("+", GUILayout.Height(buttonHeight)))
            {
                if (fontDatas == null)
                    fontDatas = new List<TMProFontData>();
                fontDatas.Add(null);
            }

            if (GUILayout.Button("Remove Duplicate and null", GUILayout.Height(buttonHeight)))
            {
                RemoveDuplicateAndNull();
            }

            if (GUILayout.Button("Refresh", GUILayout.Width(100), GUILayout.Height(30)))
            {
                RemoveDuplicateAndNull();
                Refresh();
            }

            EditorGUILayout.EndHorizontal();
            var count = fontDatas == null ? 0 : fontDatas.Count;
           
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(w), GUILayout.Height(h));
            //===
            if (fontDatas != null)
            {
                EditorGUILayout.BeginVertical();
                for (int i = 0; i < count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    fontDatas[i] = (TMProFontData) EditorGUILayout.ObjectField(fontDatas[i], typeof(TMProFontData),
                        true,
                        GUILayout.Width(w * .8f), GUILayout.Height(cellHeight));
                    if (GUILayout.Button("-", GUILayout.Width(w * .2f), GUILayout.Height(cellHeight)))
                    {
                        fontDatas.Remove(fontDatas[i]);
                        count--;
                    }

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndScrollView();
        }

        private void RemoveDuplicateAndNull()
        {
            if (fontDatas == null)
                return;
            var count = fontDatas.Count;
            // var remove=new List<SceneAsset>();
            for (var i = 0; i < count; i++)
            {
                if (fontDatas[i] == null)
                {
                    fontDatas.Remove(fontDatas[i]);
                    count--;
                    i--;
                    continue;
                }

                for (int j = i + 1; j < count; j++)
                {
                    if (fontDatas[i] == fontDatas[j])
                    {
                        fontDatas.Remove(fontDatas[j]);
                        count--;
                        i--;
                        break;
                    }
                }
            }
        }

        private void Refresh()
        {
            var c = fontDatas.Count;
            for (int i = 0; i < c; i++)
            {
                fontDatas[i].Refresh();
            }
            var dicPath = Application.dataPath + path;
            string startText = "namespace MagnasStudio.Utility.TMPro\n" +
                               "{\n" +
                               " [System.Serializable]\n" +
                               " public enum FontType\n" +
                               " {\n";

            string midText = " }\n\n" +
                             " [System.Serializable]\n" +
                             " public enum MaterialType\n" +
                             " {\n";

            string endtext = " }\n" +
                             "}";
#if UNITY_EDITOR_WIN
             var directoryPath = dicPath.Replace("/", "\\");
#else
            var directoryPath = dicPath;
#endif

            if (!Directory.Exists(dicPath))
            {
                Debug.Log("Path doesn't Exits and created");
                Directory.CreateDirectory(directoryPath);
            }
            else
            {
                Debug.Log("Path Exits");
            }

            var fileName = "TMProDataAndEnums";
            var extension = ".cs";
            var filePath = dicPath + "/" + fileName + extension;
            var file = File.Open(filePath, FileMode.OpenOrCreate);
            var fontType = "";
            var materialType = "";
            var count = fontDatas.Count;
            var matEnum = new List<string>();
            for (var i = 0; i < count; i++)
            {
                var shortName = fontDatas[i].shortName;
                //Font type
                if (i != count - 1)
                    fontType += "  " + fontDatas[i].shortName + ",\n";
                else
                    fontType += "  " + fontDatas[i].shortName + "\n";
                //Font type
                var count2 = fontDatas[i].materialPaths.Count;
                for (var j = 0; j < count2; j++)
                {
                    matEnum.Add(shortName + "_" + fontDatas[i].materialPaths[j].name);
                }
            }

            count = matEnum.Count;
            for (int i = 0; i < count; i++)
            {
                if (i != count - 1)
                    materialType += "  " + matEnum[i] + ",\n";
                else
                    materialType += "  " + matEnum[i] + "\n";
            }
            
            var filetext = startText + fontType + midText + materialType + endtext;
            var bytes = Encoding.UTF8.GetBytes(filetext);
            file.Write(bytes, 0, bytes.Length);
            file.Close();
            CreateTMProAsset();
            Save();
        }

        private void CreateTMProAsset()
        {
            var tMProAssetsPath = "Assets"+assetPath + "/TMProAssetsData.asset";
            var tMProAssetsData = AssetDatabase.LoadAssetAtPath<TMProAssetsData>(tMProAssetsPath);
            var dicPath = Application.dataPath + assetPath;
#if UNITY_EDITOR_WIN
             var directoryPath = dicPath.Replace("/", "\\");
#else
            var directoryPath = dicPath;
#endif
            if (!Directory.Exists(dicPath))
            {
                Debug.Log("Path doesn't Exits and created");
                Directory.CreateDirectory(directoryPath);
            }
            else
            {
                Debug.Log("Path Exits");
            }
            if (tMProAssetsData == null)
            {
                tMProAssetsData = ScriptableObject.CreateInstance<TMProAssetsData>();
                AssetDatabase.CreateAsset(tMProAssetsData, tMProAssetsPath);
            }
            var array = fontDatas.ToArray();
            tMProAssetsData.UpdateFontData(array);
            EditorUtility.SetDirty(tMProAssetsData);
            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = tMProAssetsData;
           
        }
        
    }
}
